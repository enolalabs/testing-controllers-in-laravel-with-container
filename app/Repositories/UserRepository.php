<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /** @var User */
    protected $userModel;

    public function __construct(User $user)
    {
        $this->userModel = $user;
    }

    /**
     * Gets all the users in the system.
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function all()
    {
        return $this->userModel->all();
    }
}
